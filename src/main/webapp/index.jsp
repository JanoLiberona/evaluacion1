<%-- 
    Document   : index
    Created on : 27-04-2021, 19:05:00
    Author     : Jano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <title>Calculadora de Interéses</title>
    </head>
    <body class="html">
        <form name="form1" action="CalculadoraController" method="POST">
            <table border="0" align="center" cellspacing="0" bordercolor="black" class="tablaCalculadora">
                <br>
                <br>
                <tr>
                    <td>
                        <table class="mastertab" border="0" cellspacing="0" width="500">    <!--Tabla maestra--> 
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Titulo-->
                                <td colspan="4">
                                    <table border="0" class="Titulo" align="center" width="260" cellspacing="1">
                                        <tr>
                                            <td class="Titulo" align="center">Calculadora de intereses</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Titulo-->
                            <tr> <!--Campo Capital-->
                                <td class="Capital" colspan="2" align="right">Capital: &nbsp;&nbsp;&nbsp;</td>
                                <td colspan="2" align="left"><input type="text" name="capital" id="capital"></td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Tasa de interés Anual-->
                                <td class="Tia" colspan="2" align="right">Tasa de interés Anual: &nbsp;&nbsp;&nbsp;</td>
                                <td colspan="2" align="left"><input type="text" name="tia" id="tia"></td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr> <!--Número de años-->
                                <td class="Anos" colspan="2" align="right">Número de años: &nbsp;&nbsp;&nbsp;</td>
                                <td colspan="2" align="left"><input type="text" name="anos" id="anos"></td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="boton" colspan="4" align="center">
                                    <button type="submit" name="accion" value="sumar" class="btn btn-succes">Calcular</button>
                                </td>
                            </tr>
                            <tr> <!--Espacio-->
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
