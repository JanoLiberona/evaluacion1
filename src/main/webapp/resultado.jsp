<%-- 
    Document   : resultado
    Created on : 27-04-2021, 20:57:56
    Author     : Jano
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="cl.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora cal = (Calculadora) request.getAttribute("Calculadora");
    DecimalFormat formatea = new DecimalFormat("###,###.##");
    String str = String.valueOf(cal.getTia());
    int porcentaje = Integer.parseInt(str.substring(0, str.indexOf('.')));
    String txtCapital = formatea.format(cal.getCapital());
    String txtResultado = formatea.format(cal.getResultado());
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/resultado.css">
        <title>Calculadora de Interéses</title>
    </head>
    <body class="html">
        <table class="mastertab" border="0" cellspacing="0" width="500">    <!--Tabla maestra--> 
            <tr> <!--Espacio-->
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr> <!--Resultado-->
            <br>
            <br>
            <td colspan="4">
                <table border="0" class="tablaResultado" align="center" width="700" cellspacing="1">
                    <tr>
                        <td class="Resultado" align="center">Un capital de $<%=txtCapital%> a una tasa anual del <%=porcentaje%>% en <%=cal.getAnos()%> años, genera un interés simple de: $<%=txtResultado%></td>
                    </tr>
                </table>
        </table>
    </form>
</body>
</html>
