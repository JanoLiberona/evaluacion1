/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

import java.text.DecimalFormat;

/**
 *
 * @author Jano
 */
public class Calculadora {

    private int capital;
    private float tia;
    private int anos;
    private int resultado;
    private float operacion;
    

    public int getCapital() {
        return capital;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public float getTia() {
        return tia;
    }

    public void setTia(float tia) {
        this.tia = tia;
    }

    public int getAnos() {
        return anos;
    }

    public void setAnos(int anos) {
        this.anos = anos;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public float getOperacion() {
        return operacion;
    }

    public void setOperacion(float operacion) {
        this.operacion = operacion;
    }

    public void calcularInteres() {
        this.operacion = this.getCapital()*this.getAnos()*(this.getTia() / 100);
        System.out.println(this.operacion);
        String str = String.valueOf(this.operacion);
        this.resultado = Integer.parseInt(str.substring(0, str.indexOf('.')));
        
        
    }
}
